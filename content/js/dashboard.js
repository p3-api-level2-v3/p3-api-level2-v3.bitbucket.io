/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 81.58236057068741, "KoPercent": 18.41763942931258};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.06582360570687419, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.24324324324324326, 500, 1500, "getUpcomingEvents"], "isController": false}, {"data": [0.0, 500, 1500, "findAllFeeGroup"], "isController": false}, {"data": [0.0, 500, 1500, "findAllChildActiveSubsidies"], "isController": false}, {"data": [0.0, 500, 1500, "getListDomainByLevel"], "isController": false}, {"data": [0.0, 500, 1500, "getPendingEvents"], "isController": false}, {"data": [0.1357142857142857, 500, 1500, "getPortfolioTermByYear"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCurrentFeeTier"], "isController": false}, {"data": [0.17647058823529413, 500, 1500, "findAllClassResources"], "isController": false}, {"data": [0.0, 500, 1500, "listBulkInvoiceRequest"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCustomSubsidies"], "isController": false}, {"data": [0.0, 500, 1500, "findAllUploadedSubsidyFiles"], "isController": false}, {"data": [0.0, 500, 1500, "getAllChildDiscounts"], "isController": false}, {"data": [0.0, 500, 1500, "getChildFinancialAssistanceStatus"], "isController": false}, {"data": [0.02564102564102564, 500, 1500, "getMyDownloadPortfolio"], "isController": false}, {"data": [0.11764705882352941, 500, 1500, "getListDomain"], "isController": false}, {"data": [0.3125, 500, 1500, "getLessonPlan"], "isController": false}, {"data": [0.0, 500, 1500, "getAdvancePaymentReceipts"], "isController": false}, {"data": [0.0, 500, 1500, "findAllProgramBillingUpload"], "isController": false}, {"data": [0.0, 500, 1500, "findAllChildHistorySubsidiesForBillingAdjustment"], "isController": false}, {"data": [0.0, 500, 1500, "getChildSemesterEvaluation"], "isController": false}, {"data": [0.0, 500, 1500, "getCentreManagementConfig"], "isController": false}, {"data": [0.09523809523809523, 500, 1500, "updateClassResourceOrder"], "isController": false}, {"data": [0.0, 500, 1500, "getChildPortfolio"], "isController": false}, {"data": [0.26744186046511625, 500, 1500, "getStaffCheckInOutRecordsByRole"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCentreForSchool"], "isController": false}, {"data": [0.0, 500, 1500, "portfolioByID"], "isController": false}, {"data": [0.0, 500, 1500, "findAllAbsentForVoidingSubsidyByChild"], "isController": false}, {"data": [0.0, 500, 1500, "invoicesByFkChild"], "isController": false}, {"data": [0.2441860465116279, 500, 1500, "getCountStaffCheckInOutRecordsByRole"], "isController": false}, {"data": [0.0, 500, 1500, "findAllFeeDraft"], "isController": false}, {"data": [0.0, 500, 1500, "getPastEvents"], "isController": false}, {"data": [0.0, 500, 1500, "findAllConsolidatedRefund"], "isController": false}, {"data": [0.06060606060606061, 500, 1500, "getMyDownloadAlbum"], "isController": false}, {"data": [0.0, 500, 1500, "getRefundChildBalance"], "isController": false}, {"data": [0.0, 500, 1500, "findAllUploadedGiroFiles"], "isController": false}, {"data": [0.0, 500, 1500, "findAllInvoice"], "isController": false}, {"data": [0.9411764705882353, 500, 1500, "getAllArea"], "isController": false}, {"data": [0.0, 500, 1500, "getChildChecklist"], "isController": false}, {"data": [0.0, 500, 1500, "bankAccountInfoByIDChild"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCreditDebitNotes"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 1542, 284, 18.41763942931258, 106668.93450064842, 2, 383485, 77276.0, 269303.6, 332357.85, 377527.89, 4.019655122974857, 78.97518711794378, 7.157839219497412], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getUpcomingEvents", 37, 0, 0.0, 8780.054054054053, 71, 70928, 4339.0, 24877.800000000017, 35329.40000000005, 70928.0, 0.12095772364102363, 0.07051929786493272, 0.1736404821799851], "isController": false}, {"data": ["findAllFeeGroup", 35, 1, 2.857142857142857, 72845.31428571428, 4206, 201823, 64457.0, 163686.59999999998, 182769.3999999999, 201823.0, 0.09127204545869419, 0.08277895752329394, 0.09831354115326141], "isController": false}, {"data": ["findAllChildActiveSubsidies", 31, 3, 9.67741935483871, 93532.25806451614, 10597, 348158, 77996.0, 157498.4, 316368.19999999995, 348158.0, 0.08082915064207026, 0.08551430806335963, 0.13174204338048368], "isController": false}, {"data": ["getListDomainByLevel", 42, 1, 2.380952380952381, 105228.45238095237, 15670, 361143, 86118.5, 201382.40000000002, 232744.30000000005, 361143.0, 0.10956502684343158, 0.566182778921254, 0.19484171277528212], "isController": false}, {"data": ["getPendingEvents", 42, 4, 9.523809523809524, 92098.64285714286, 4791, 225939, 67594.0, 192080.90000000002, 223248.05000000002, 225939.0, 0.10948591150931543, 0.0549822525292549, 0.1424172208304767], "isController": false}, {"data": ["getPortfolioTermByYear", 70, 1, 1.4285714285714286, 43913.814285714274, 234, 243857, 26812.0, 111674.4, 146889.40000000008, 243857.0, 0.1824945968564002, 0.18981678520125245, 0.231456002996822], "isController": false}, {"data": ["findAllCurrentFeeTier", 36, 22, 61.111111111111114, 220767.1666666667, 70411, 378536, 216608.5, 375618.9, 377040.0, 378536.0, 0.09384457773850209, 0.396492322666333, 0.2375440874005834], "isController": false}, {"data": ["findAllClassResources", 34, 0, 0.0, 14538.529411764704, 259, 197419, 3719.5, 34265.5, 135778.75, 197419.0, 0.10783654517082578, 0.06329078481217412, 0.14974957737589284], "isController": false}, {"data": ["listBulkInvoiceRequest", 29, 4, 13.793103448275861, 136237.1379310345, 23749, 375203, 117897.0, 257208.0, 336991.5, 375203.0, 0.07559899166586288, 0.1083171436354773, 0.11450589460327473], "isController": false}, {"data": ["findAllCustomSubsidies", 34, 2, 5.882352941176471, 70556.20588235294, 3399, 257678, 55339.5, 132051.0, 256549.25, 257678.0, 0.08879208810264366, 0.10307135341993175, 0.11099011012830458], "isController": false}, {"data": ["findAllUploadedSubsidyFiles", 36, 2, 5.555555555555555, 71854.47222222223, 11850, 318671, 55230.0, 134991.40000000008, 173838.64999999976, 318671.0, 0.09403160506725872, 7.440345129456706, 0.13544591550215487], "isController": false}, {"data": ["getAllChildDiscounts", 35, 3, 8.571428571428571, 79609.51428571428, 9102, 302046, 59133.0, 182778.5999999999, 258401.99999999977, 302046.0, 0.09123873527471983, 0.05515819411822976, 0.15031225235200427], "isController": false}, {"data": ["getChildFinancialAssistanceStatus", 40, 13, 32.5, 162134.82499999995, 56028, 383476, 142303.0, 279336.1, 310334.9499999999, 383476.0, 0.10430791697089808, 0.0687169148325858, 0.13680227782413684], "isController": false}, {"data": ["getMyDownloadPortfolio", 39, 2, 5.128205128205129, 44319.282051282054, 1282, 275921, 24975.0, 99364.0, 201431.0, 275921.0, 0.1016827186308813, 0.05948805684324696, 0.10436380593853149], "isController": false}, {"data": ["getListDomain", 68, 5, 7.352941176470588, 92840.69117647062, 143, 366160, 72644.5, 209155.90000000005, 286828.55, 366160.0, 0.17740626818088134, 0.746965178802038, 0.2949476023738524], "isController": false}, {"data": ["getLessonPlan", 32, 0, 0.0, 8931.15625, 198, 87456, 3385.5, 28006.59999999999, 59496.89999999991, 87456.0, 0.09787249660504778, 0.06375093284723327, 0.15694007756395356], "isController": false}, {"data": ["getAdvancePaymentReceipts", 34, 4, 11.764705882352942, 94305.32352941175, 5220, 350520, 71744.5, 190552.5, 258135.0, 350520.0, 0.0887723823896481, 0.12335730296447538, 0.14156767621317903], "isController": false}, {"data": ["findAllProgramBillingUpload", 36, 5, 13.88888888888889, 101256.13888888889, 7680, 256413, 97145.5, 197136.00000000003, 211308.59999999992, 256413.0, 0.09396631829524217, 0.12027454233835183, 0.14012360159847148], "isController": false}, {"data": ["findAllChildHistorySubsidiesForBillingAdjustment", 37, 8, 21.62162162162162, 156700.5945945946, 2, 350000, 147366.0, 277107.00000000006, 326748.50000000006, 350000.0, 0.09645841208597834, 0.1278185978987708, 0.16873093642869247], "isController": false}, {"data": ["getChildSemesterEvaluation", 45, 0, 0.0, 56394.200000000004, 2579, 213690, 41106.0, 128497.79999999999, 147769.49999999994, 213690.0, 0.11897774323016641, 0.0916732806724622, 0.18241704772593875], "isController": false}, {"data": ["getCentreManagementConfig", 29, 1, 3.4482758620689653, 39208.51724137931, 1828, 257016, 16525.0, 102614.0, 219812.5, 257016.0, 0.07564651686917326, 0.08755034813700888, 0.12248234860262623], "isController": false}, {"data": ["updateClassResourceOrder", 21, 0, 0.0, 19352.380952380954, 262, 64130, 7845.0, 55605.6, 63289.09999999999, 64130.0, 0.06425909266161162, 0.03790282418712248, 0.06112144165274386], "isController": false}, {"data": ["getChildPortfolio", 36, 21, 58.333333333333336, 261439.33333333328, 103912, 383096, 251730.0, 362745.9000000001, 382571.55, 383096.0, 0.09396582776063772, 0.10857151876184361, 0.22729819859677697], "isController": false}, {"data": ["getStaffCheckInOutRecordsByRole", 43, 0, 0.0, 17590.674418604653, 23, 349628, 4775.0, 36879.8, 50297.39999999997, 349628.0, 0.1128010870876858, 0.06752643201635879, 0.20588401539734838], "isController": false}, {"data": ["findAllCentreForSchool", 74, 57, 77.02702702702703, 253832.43243243246, 55966, 383485, 263039.0, 376682.0, 377643.0, 383485.0, 0.19290324598813385, 40.6952150911924, 0.4745955467373283], "isController": false}, {"data": ["portfolioByID", 34, 6, 17.647058823529413, 92799.08823529411, 9871, 291041, 77816.0, 175181.5, 247436.0, 291041.0, 0.08863214531500908, 0.12711594990850034, 0.31151083104366956], "isController": false}, {"data": ["findAllAbsentForVoidingSubsidyByChild", 37, 4, 10.81081081081081, 134311.2162162162, 15276, 376461, 112320.0, 299426.2, 375628.5, 376461.0, 0.09653693320426954, 0.05945144273794397, 0.12849593745841736], "isController": false}, {"data": ["invoicesByFkChild", 34, 12, 35.294117647058826, 163793.50000000003, 35111, 378390, 153126.5, 307654.0, 378336.0, 378390.0, 0.08863145217420773, 0.18012173709174137, 0.2575851578812912], "isController": false}, {"data": ["getCountStaffCheckInOutRecordsByRole", 43, 0, 0.0, 10890.139534883721, 183, 84304, 5244.0, 28805.600000000002, 57801.99999999994, 84304.0, 0.1279327847099496, 0.07720943452221568, 0.1398015489164391], "isController": false}, {"data": ["findAllFeeDraft", 32, 4, 12.5, 89909.78125, 35955, 342872, 74056.5, 150407.69999999998, 248204.6999999997, 342872.0, 0.08341914208626061, 0.04703528043169406, 0.13286779369403423], "isController": false}, {"data": ["getPastEvents", 26, 2, 7.6923076923076925, 82398.46153846155, 5931, 271953, 68275.5, 195157.60000000006, 268739.64999999997, 271953.0, 0.06868059297767352, 0.034262906998552424, 0.08652145013788949], "isController": false}, {"data": ["findAllConsolidatedRefund", 25, 2, 8.0, 103759.12000000002, 35625, 288131, 94292.0, 199477.80000000002, 261880.09999999995, 288131.0, 0.0651706950845655, 0.0928733319560593, 0.11258492148886363], "isController": false}, {"data": ["getMyDownloadAlbum", 33, 0, 0.0, 39786.57575757576, 474, 283222, 17751.0, 88392.40000000001, 237887.1999999998, 283222.0, 0.0894173242001214, 0.0585054757950013, 0.14119903635898076], "isController": false}, {"data": ["getRefundChildBalance", 32, 3, 9.375, 89185.0, 4031, 367777, 69925.5, 184354.8, 268583.74999999965, 367777.0, 0.08460102684496333, 0.07737193519561344, 0.13243695901609007], "isController": false}, {"data": ["findAllUploadedGiroFiles", 38, 3, 7.894736842105263, 81598.47368421055, 18297, 253471, 56116.5, 160105.2, 203264.44999999984, 253471.0, 0.09906746233481152, 26.062263289052783, 0.1417322581254872], "isController": false}, {"data": ["findAllInvoice", 69, 41, 59.42028985507246, 207922.884057971, 23571, 383337, 169436.0, 376944.0, 378259.5, 383337.0, 0.17998747913188648, 0.16016379023633137, 0.5788940022107157], "isController": false}, {"data": ["getAllArea", 34, 0, 0.0, 435.44117647058806, 48, 4952, 174.0, 261.0, 4701.5, 4952.0, 0.12655824843384167, 0.10040762690256132, 0.11901913402518509], "isController": false}, {"data": ["getChildChecklist", 33, 14, 42.42424242424242, 208272.21212121213, 97281, 375828, 190996.0, 341760.80000000005, 375354.1, 375828.0, 0.08607423250476016, 0.1451229084613579, 0.28343975781840947], "isController": false}, {"data": ["bankAccountInfoByIDChild", 40, 14, 35.0, 176971.02500000005, 57926, 331183, 164871.0, 298921.89999999997, 325432.49999999994, 331183.0, 0.1043653413138031, 0.123742744162716, 0.2402237397232753], "isController": false}, {"data": ["findAllCreditDebitNotes", 37, 20, 54.054054054054056, 252199.54054054045, 100498, 377484, 268226.0, 371437.6, 377367.0, 377484.0, 0.09683252944886588, 0.12043239159336619, 0.22619473675946014], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["502/Bad Gateway", 283, 99.64788732394366, 18.35278858625162], "isController": false}, {"data": ["Non HTTP response code: javax.net.ssl.SSLException/Non HTTP response message: java.net.SocketException: Connection reset", 1, 0.352112676056338, 0.0648508430609598], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 1542, 284, "502/Bad Gateway", 283, "Non HTTP response code: javax.net.ssl.SSLException/Non HTTP response message: java.net.SocketException: Connection reset", 1, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": ["findAllFeeGroup", 35, 1, "502/Bad Gateway", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllChildActiveSubsidies", 31, 3, "502/Bad Gateway", 3, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getListDomainByLevel", 42, 1, "502/Bad Gateway", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getPendingEvents", 42, 4, "502/Bad Gateway", 4, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getPortfolioTermByYear", 70, 1, "502/Bad Gateway", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllCurrentFeeTier", 36, 22, "502/Bad Gateway", 22, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["listBulkInvoiceRequest", 29, 4, "502/Bad Gateway", 4, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllCustomSubsidies", 34, 2, "502/Bad Gateway", 2, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllUploadedSubsidyFiles", 36, 2, "502/Bad Gateway", 2, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getAllChildDiscounts", 35, 3, "502/Bad Gateway", 3, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getChildFinancialAssistanceStatus", 40, 13, "502/Bad Gateway", 13, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getMyDownloadPortfolio", 39, 2, "502/Bad Gateway", 2, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getListDomain", 68, 5, "502/Bad Gateway", 5, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["getAdvancePaymentReceipts", 34, 4, "502/Bad Gateway", 4, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllProgramBillingUpload", 36, 5, "502/Bad Gateway", 5, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllChildHistorySubsidiesForBillingAdjustment", 37, 8, "502/Bad Gateway", 7, "Non HTTP response code: javax.net.ssl.SSLException/Non HTTP response message: java.net.SocketException: Connection reset", 1, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["getCentreManagementConfig", 29, 1, "502/Bad Gateway", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["getChildPortfolio", 36, 21, "502/Bad Gateway", 21, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["findAllCentreForSchool", 74, 57, "502/Bad Gateway", 57, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["portfolioByID", 34, 6, "502/Bad Gateway", 6, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllAbsentForVoidingSubsidyByChild", 37, 4, "502/Bad Gateway", 4, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["invoicesByFkChild", 34, 12, "502/Bad Gateway", 12, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["findAllFeeDraft", 32, 4, "502/Bad Gateway", 4, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getPastEvents", 26, 2, "502/Bad Gateway", 2, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllConsolidatedRefund", 25, 2, "502/Bad Gateway", 2, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["getRefundChildBalance", 32, 3, "502/Bad Gateway", 3, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllUploadedGiroFiles", 38, 3, "502/Bad Gateway", 3, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllInvoice", 69, 41, "502/Bad Gateway", 41, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["getChildChecklist", 33, 14, "502/Bad Gateway", 14, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["bankAccountInfoByIDChild", 40, 14, "502/Bad Gateway", 14, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllCreditDebitNotes", 37, 20, "502/Bad Gateway", 20, null, null, null, null, null, null, null, null], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
